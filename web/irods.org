** IRODS Storage Resource
:PROPERTIES:
:CUSTOM_ID: irods
:END:

*** Introduction

An iRODS Storage Resource is available at the MCIA (mésocentre Aquitain).

It allows to backup your research data.

*** Information

IMPORTANT : Encryption is  not available (without authentication). Data are unencrypted both on  the disks and on the network. If necessary, you need to encrypt data by  yourself.

Data are scattered over 7 sites (Bordeaux and Pau).

IRODS keeps 3 copies of every file:

- one nearby the storage resource the data was first copied on,
- one at the MCIA (near Avakas),
- one in another storage resource.

Default quota : 500Gb.

Help [[https://redmine.mcia.univ.bordeaux.fr/projects/irods]]

*** How to use the system

One needs an account at the mesocentre. To do so, go to [[https://redmine.mcia.univ-bordeaux.fr/projects/cluster-avakas/wiki/Comptes_Utilisateurs][the page]] to
request a Avakas account at *inscriptions@mcia.univ-bordeaux.fr*

- Connect to the mesocentre to initialize your IRODS account, choose a
  specific password for iRODS. When loading the module, the iRODS
  account will be initialised.

  #+begin_src sh :eval never-export
  $ ssh VOTRELOGIN@avakas.mcia.univ-bordeaux.fr
  $ module load irods/mcia
  #+end_src

- *On PlaFRIM* - Prepare your environment by calling the command ~iiint~
  and answer the given questions.

  #+begin_src sh :eval never-export
  $ module load tools/irods
  $ iinit
  #+end_src

    - Host: ~icat0.mcia.univ-bordeaux.fr~
    - Port: ~1247~
    - Zone: ~MCI~
    - Default Resource : ~siterg-imb~ (for PlaFRIM) (from another
      platform, choose ~siterg-ubx~)
    - Password : ...

The IRODS password can only be changed from *Avakas*

#+begin_src sh :eval never-export
$ module load irods/mcia
$ mcia-irods-password
#+end_src

*** Basic commands

The list of all available commands can be found [[https://redmine.mcia.univ-bordeaux.fr/projects/irods/wiki/ICommands][here]].

**** FTP

- ~icd [irods_path]~ (change the working directory)
- ~imkdir irods_path~ (create a directory)
- ~ils [irods_path]~ (list directory contents)
- ~iput locale_file~ [irods_path] (upload a file)
- ~iget irods_file~ [locale_path] (download a file)

**** rsync

- ~irsync locale_path irods_path~
  - Irods path start with « i: » : i:foo/bar
  - ~irsync foo i:bar/zzz~
  - ~irsync i:bar/zzz foo

*** To go further

- [[https://irods.org]]
- [[https://docs.irods.org]]
